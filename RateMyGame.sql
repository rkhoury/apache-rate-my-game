-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 11, 2015 at 05:12 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `RateMyGame`
--

-- --------------------------------------------------------

--
-- Table structure for table `Game`
--

CREATE TABLE `Game` (
  `id_game` char(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `artwork` varchar(200) DEFAULT NULL,
  `ave_rating` double DEFAULT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `pub_name` varchar(100) DEFAULT NULL,
  `profile_text` varchar(5000) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `Game`:
--

--
-- Dumping data for table `Game`
--

INSERT INTO `Game` (`id_game`, `name`, `artwork`, `ave_rating`, `genre`, `date`, `pub_name`, `profile_text`, `website`) VALUES
('ffvii', 'Final Fantasy VII', 'https://upload.wikimedia.org/wikipedia/en/c/c2/Final_Fantasy_VII_Box_Art.jpg', NULL, 'RPG', '1997-01-31', 'Square', 'Released in 1997, it is the seventh installment in the Final Fantasy series and the first in the ser', 'http://na.square-enix.com/ff7psn/'),
('mgs5g', 'Metal Gear Solid V: Ground Zeroes', 'https://upload.wikimedia.org/wikipedia/en/0/07/MGSV_Ground_Zeroes_boxart.jpg', 4, 'Adventure', '2014-03-18', 'Konami', 'Ground Zeroes stands alone as the first part of Metal Gear Solid V, acting as the prologue for Metal Gear Solid V: The Phantom Pain.The game follows Snake as he infiltrates an American black site in Cuba called Camp Omega, attempting to rescue Cipher agent Paz Ortega Andrade and former Sandinista child soldier Ricardo "Chico" Valenciano Libre. The game offers players new sneaking and traversal methods as well as the choice in what order the story events take place by selecting missions in any order they chose.', 'http://www.konami.jp/mgs5/gz/certification.php5'),
('mgs5p', 'Metal Gear Solid V: The Phantom Pain', 'https://upload.wikimedia.org/wikipedia/en/8/8f/Metal_Gear_Solid_V_The_Phantom_Pain_cover.png', 4, 'Adventure', '2015-09-01', 'Konami', 'The game is the eleventh canonical and final instalment in the Metal Gear series and the fifth within the series'' chronology. It serves as a sequel to Metal Gear Solid V: Ground Zeroes, and a continuation of the narrative established there, and a prequel to the original Metal Gear game. It carries over the tagline of Tactical Espionage Operations first used in Metal Gear Solid: Peace Walker. Set in 1984, the game follows the mercenary leader Punished "Venom" Snake as he ventures into Afghanistan and the Angola Zaire border region to exact revenge on the people who destroyed his forces and came close to killing him during the climax of Ground Zeroes.', 'http://www.konami.jp/mgs5/'),
('pong1', 'Pong', 'https://www.codingsec.net/wp-content/uploads/2015/03/pong.png', 10, 'Action', '1972-11-29', 'Atari Inc.', 'Pong (marketed as PONG) is one of the earliest arcade video games and the very first sports arcade video game. It is a tennis sports game featuring simple two-dimensional graphics. While other arcade video games such as Computer Space came before it, Pong was one of the first video games to reach mainstream popularity. The aim is to defeat an opponent in a simulated table-tennis game by earning a higher score. The game was originally manufactured by Atari Incorporated (Atari), which released it in 1972. Allan Alcorn created Pong as a training exercise assigned to him by Atari co-founder Nolan Bushnell. Bushnell based the idea on an electronic ping-pong game included in the Magnavox Odyssey, which later resulted in a lawsuit against Atari. Surprised by the quality of Alcorn''s work, Bushnell and Atari co-founder Ted Dabney decided to manufacture the game.', 'https://en.wikipedia.org/wiki/Pong');

-- --------------------------------------------------------

--
-- Table structure for table `Game_Add_Request`
--

CREATE TABLE `Game_Add_Request` (
  `title` varchar(200) NOT NULL,
  `id_change` int(6) NOT NULL,
  `artwork` varchar(200) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `profile_text` varchar(100) DEFAULT NULL,
  `platform` varchar(100) DEFAULT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `change_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `admin_comment` varchar(100) DEFAULT NULL,
  `admin_date` date DEFAULT NULL,
  `admin` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pub_name` varchar(100) DEFAULT NULL,
  `Platform2` varchar(100) DEFAULT NULL,
  `Platform3` varchar(100) DEFAULT NULL,
  `rel_date` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `Game_Add_Request`:
--   `username`
--       `User` -> `username`
--

--
-- Dumping data for table `Game_Add_Request`
--

INSERT INTO `Game_Add_Request` (`title`, `id_change`, `artwork`, `website`, `profile_text`, `platform`, `genre`, `change_date`, `status`, `admin_comment`, `admin_date`, `admin`, `username`, `pub_name`, `Platform2`, `Platform3`, `rel_date`) VALUES
('testing', 23, 'test', 'test', 'test', 'Mac', 'MMO', '2015-12-09', 'Rejected', '', '2015-12-10', 'admin', 'Benny', 'PS4', 'PC', 'Mac', '2015-12-18'),
('Final Fantasy VII', 31, 'https://upload.wikimedia.org/wikipedia/en/c/c2/Final_Fantasy_VII_Box_Art.jpg', 'http://na.square-enix.com/ff7psn/', 'Released in 1997, it is the seventh installment in the Final Fantasy series and the first in the ser', 'PS4', 'RPG', '2015-12-10', 'Approved', 'Great!', '2015-12-10', 'admin', 'Blue', 'Square', '', '', '1997-01-31'),
('Resident evil 6', 32, 'https://upload.wikimedia.org/wikipedia/en/1/11/Resident_Evil_6_box_artwork.png', 'http://www.residentevil.com/6/', 'Resident Evil 6 allows players to select between four scenarios with connected storylines, each with', 'PS3', 'Adventure', '2015-12-10', 'Pending', NULL, NULL, '', 'Blue', 'Capcom', 'PS4', 'XBoxOne', '2012-10-12');

-- --------------------------------------------------------

--
-- Table structure for table `Media`
--

CREATE TABLE `Media` (
  `id_game` char(5) NOT NULL,
  `medialocation` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `Media`:
--   `id_game`
--       `Game` -> `id_game`
--

-- --------------------------------------------------------

--
-- Table structure for table `Platform`
--

CREATE TABLE `Platform` (
  `system` varchar(30) NOT NULL,
  `id_game` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `Platform`:
--   `id_game`
--       `Game` -> `id_game`
--

--
-- Dumping data for table `Platform`
--

INSERT INTO `Platform` (`system`, `id_game`) VALUES
('', 'ffvii'),
('PS4', 'ffvii'),
('PC', 'mgs5g'),
('PS4', 'mgs5g'),
('XBoxOne', 'mgs5g'),
('PC', 'mgs5p'),
('PS4', 'mgs5p'),
('XBoxOne', 'mgs5p'),
('PC', 'pong1'),
('Web', 'pong1');

-- --------------------------------------------------------

--
-- Table structure for table `Rates`
--

CREATE TABLE `Rates` (
  `rating` double DEFAULT NULL,
  `body` varchar(3000) DEFAULT NULL,
  `time` time DEFAULT NULL,
  `date` date DEFAULT NULL,
  `username` varchar(100) NOT NULL DEFAULT '',
  `id_game` char(5) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `Rates`:
--   `username`
--       `User` -> `username`
--   `id_game`
--       `Game` -> `id_game`
--

--
-- Dumping data for table `Rates`
--

INSERT INTO `Rates` (`rating`, `body`, `time`, `date`, `username`, `id_game`) VALUES
(3, 'This game was okay, I had some fun though. This game was okay, I had some fun though. This game was okay, I had some fun though. This game was okay, I had some fun though. This game was okay, I had some fun though. This game was okay, I had some fun though.', '03:09:00', '2015-12-09', 'Benny', 'mgs5g'),
(5, 'It was ok, too many cutscenes scenes.', '20:04:53', '2015-12-09', 'bofbof', 'mgs5g'),
(4, 'I really liked it but it was too short!', '17:37:48', '2015-12-10', 'Blue', 'mgs5p'),
(10, 'This game is oldie but goldie! Classic fun!', '17:51:38', '2015-12-10', 'Blue', 'pong1');

--
-- Triggers `Rates`
--
DELIMITER $$
CREATE TRIGGER `Ave_Del_Recalc` AFTER DELETE ON `rates`
 FOR EACH ROW UPDATE Game g 
SET g.ave_rating = ( SELECT avg(rating) 
	FROM Rates rat 
	WHERE rat.id_game = OLD.id_game ) 
WHERE g.id_game = OLD.id_game
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Ave_Ins_Recalc` AFTER INSERT ON `rates`
 FOR EACH ROW UPDATE Game g 
SET g.ave_rating = ( SELECT avg(rating) 
	FROM Rates rat 
	WHERE rat.id_game = NEW.id_game ) 
WHERE g.id_game = NEW.id_game
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Ave_Upd_Recalc` AFTER UPDATE ON `rates`
 FOR EACH ROW UPDATE Game g 
SET g.ave_rating = ( SELECT avg(rating) 
	FROM Rates rat 
	WHERE rat.id_game = NEW.id_game ) 
WHERE g.id_game = NEW.id_game
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Sessions`
--

CREATE TABLE `Sessions` (
  `username` varchar(100) NOT NULL,
  `sessionID` int(6) NOT NULL,
  `Admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `Sessions`:
--   `username`
--       `User` -> `username`
--

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `SharedKey` int(2) NOT NULL,
  `password` varchar(200) NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `User`:
--

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`username`, `email`, `SharedKey`, `password`, `admin`) VALUES
('admin', 'admin@ratemygame.com', 9378, 'sveaf', 1),
('Benny', '', 0, 'Hill', 0),
('Bigboss', 'bigboss@boss.com', 13063, 'loxty', 1),
('Blue', '', 0, 'Hour', 0),
('bofbof', 'bof@bof.com', 14108, 'revrev', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Game`
--
ALTER TABLE `Game`
  ADD PRIMARY KEY (`id_game`);

--
-- Indexes for table `Game_Add_Request`
--
ALTER TABLE `Game_Add_Request`
  ADD PRIMARY KEY (`id_change`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `Media`
--
ALTER TABLE `Media`
  ADD KEY `id_game` (`id_game`);

--
-- Indexes for table `Platform`
--
ALTER TABLE `Platform`
  ADD PRIMARY KEY (`id_game`,`system`);

--
-- Indexes for table `Rates`
--
ALTER TABLE `Rates`
  ADD PRIMARY KEY (`id_game`,`username`),
  ADD UNIQUE KEY `username_2` (`username`,`id_game`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `Sessions`
--
ALTER TABLE `Sessions`
  ADD PRIMARY KEY (`sessionID`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Game_Add_Request`
--
ALTER TABLE `Game_Add_Request`
  MODIFY `id_change` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `Sessions`
--
ALTER TABLE `Sessions`
  MODIFY `sessionID` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Game_Add_Request`
--
ALTER TABLE `Game_Add_Request`
  ADD CONSTRAINT `Game_Add_Request_ibfk_1` FOREIGN KEY (`username`) REFERENCES `User` (`username`);

--
-- Constraints for table `Media`
--
ALTER TABLE `Media`
  ADD CONSTRAINT `Media_ibfk_1` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id_game`);

--
-- Constraints for table `Platform`
--
ALTER TABLE `Platform`
  ADD CONSTRAINT `Platform_ibfk_1` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id_game`);

--
-- Constraints for table `Rates`
--
ALTER TABLE `Rates`
  ADD CONSTRAINT `Rates_ibfk_1` FOREIGN KEY (`username`) REFERENCES `User` (`username`),
  ADD CONSTRAINT `Rates_ibfk_2` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id_game`);

--
-- Constraints for table `Sessions`
--
ALTER TABLE `Sessions`
  ADD CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`username`) REFERENCES `User` (`username`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
