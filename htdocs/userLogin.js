var sessionid = getCookie("sessionid");

window.onload = function() {
    if (sessionid != "") 
      {
        getUsername();
      }
};

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}



// jQuery('a').click(function(event){
//   event.preventDefault();
//   alert("hello");
//    var link = $(this).attr("href");
//    alert(link);
//    if (link == "members.html")
//    {
//     if (sessionid != "") window.location.href = "membershomepage.html";
//     }
//  });

jQuery(document).ready(function() {
    var offset = 220;
    var duration = 500;
    if (sessionid != "")
    	{
        jQuery('.logout').fadeIn(duration);
        isAdmin(duration);
    	}
    
    jQuery('.logout').click(function(event) {
        event.preventDefault();
        document.cookie = "sessionid=; expires=Thu, 01 Jan 1970 00:00:00 UTC"; 
        myAjax();
        return false;
    })
    jQuery('a').click(function(event){
        var link = $(this).attr("href");
        if (link == "members.html")
        {
          if (sessionid != "") 
            {
              event.preventDefault();
              window.location.href = "memberhomepage.html";
            }
        }
    })
    jQuery('.admin').click(function(event) {
        event.preventDefault();
        window.location.href = "adminpage.php";
    })
});

function myAjax() {
    $.ajax
    ({ 
        url: 'logout.php',
        data: {"sessionid": sessionid},
        type: 'get',
        success: function()
        {
            window.location.href = "index.html";
        }
    });
 }

 function getUsername()
 {
 	  var xhttp;
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
          if (xhttp.responseText != "")
          {
            document.getElementById("loggedinuser").innerHTML = xhttp.responseText;
          }
          else
          {
            document.getElementById("loggedinuser").innerHTML = "error";
          }
        }
      };
      xhttp.open("GET", "getUserNameUsingSessionID.php?sessionid="+sessionid, true);
      xhttp.send();   
 }

  function isAdmin(duration)
 {
    var xhttp;
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
          if (xhttp.responseText == "1")
          {
            jQuery('.admin').fadeIn(duration);
          }
          else
          {
            
          }
        }
      };
      xhttp.open("GET", "getAdminUsingSessionID.php?sessionid="+sessionid, true);
      xhttp.send();
 }

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}