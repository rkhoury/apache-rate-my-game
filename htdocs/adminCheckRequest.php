<?php
$id_change = $_GET['addreq'];


$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "RateMyGame";

$dbconn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (mysqli_connect_errno()) {
        echo "Failed to connect to Database: " . mysqli_connect_error(); }

$get_add_request = mysqli_query($dbconn, "SELECT * FROM Game_Add_Request WHERE id_change = '" . $id_change . "'");
$add_request_row = mysqli_fetch_array($get_add_request);


$change_date = $add_request_row["change_date"];

$title = $add_request_row["title"];
$pub_name = $add_request_row["pub_name"];
$artwork = $add_request_row["artwork"];
$genre = $add_request_row["genre"];
$rel_date = $add_request_row["rel_date"];
$profile_text = $add_request_row["profile_text"];
$website = $add_request_row["website"];

$platform =$add_request_row["platform"];
$platform2 =$add_request_row["Platform2"];
$platform3 =$add_request_row["Platform3"];

$username = $add_request_row["username"];


echo <<<EOD
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title>Rate My Game - Admin Approval Form</title>
  <link href="css/style.css" rel="stylesheet" type="text/css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="userLogin.js"></script>

<script src="userLogin.js"></script>
<script>
    function addusername(){
    var user = document.getElementById("loggedinuser").innerHTML;
    (document.getElementById("userid")).value = user;
}
</script>
 
<style>
    .logout {
    position: fixed;
    bottom: 2em;
    right: 0px;
    text-decoration: none;
    color: #000000;
    background-color: rgba(235, 235, 235, 0.80);
    font-size: 12px;
    padding: 1em;
    display: none;
}

.logout:hover {    
    background-color: rgba(135, 135, 135, 0.50);
}
</style>
 </head>
<body>
<div class="page">
  <header class="masthead" role="banner">
        <p class="logo"><a href="index.html"><img src="images/banner2.png" width="95%"/></a></p>
        <nav role="navigation">
            <ul class="nav-main">
                <li><a href="index.html">Home<a></li>
                <li><a href="games.html">Games</a></li>
                <li><a href="members.html">Members</a></li>
                <li><a href="contact.html">Contact</a></li>
           <li><div class="search">
          <form action="searchresult.php" method="post">
              SEARCH: <input type="text" name="searchfield"/>
                <input id="submit" type="submit" name="submit" value="submit"/>
            </form>
        </div></li>
        </ul>
        </nav>
    </header>
    <div class="container">
    <main class="main">
<form action="adminProcessRequest.php" method="post" id="entergameform" onclick="addusername()">
            
            Add Request ID: $id_change <br>
            Request User: $username <br>  
            Date of Request: <input type="date" name="change_date" value="$change_date" readonly><br>
          <br>
            Game Name:
          <input type="text" name="name" value="$title" ><br>

         Game Website:
          <input type="text" name="website" value="$website"><br>

          <a href="$website" target="_blank">Check Website Address</a><br>

         Publisher Name:
          <input type="text" name="pub_name" value="$pub_name"><br>

          Genre:
          <input type="text" name="genre" value="$genre"><br>

          Artwork:
          <input type="text" name="artwork" value="$artwork"><br>
          <img src="$artwork" alt="artwork"><br>


          Release Date:
          <input type="date" name="rel_date" value="$rel_date"><br>

          Systems:
          <input type="text" name="platform" value="$platform"><br>
          <input type="text" name="Platform2" value="$platform2"><br>
          <input type="text" name="Platform3" value="$platform3"><br>

          Profile:
          <textarea id="profile" name="profile_text" width="100">$profile_text</textarea>
          <br><br>

          Admin Assign id_game value:
          <input type="text" maxlength="5" name="id_game"><br>

          <input type="hidden" name="id_change" value="$id_change">
          <input id="userid" type="hidden" name="admin">

          
          Admin Comments:
          <textarea id="admin_comment" name="admin_comment" width="100"></textarea><br>

          Decision:
          <select name="decision" form="entergameform">
            <option value="Approved">Approved</option>
            <option value="Rejected">Rejected</option>
          </select>
         
          <input type="submit" value="Submit" id="submit">
</form>
</main>
</div>
    <footer class="footer">
        <a href="#" class="logout">Logout, <span id="loggedinuser"></span></a>
        <p>Copyright &copy; RateMyGame.com 2015</p>
    </footer>
</div>
</body>
</html>
 
EOD;

mysqli_close($dbcon);

?>
