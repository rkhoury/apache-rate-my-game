// JavaScript Document
function start()
{
	var button = document.getElementById("submit");
	button.addEventListener("click", register, false);	
}

function register()
{
	var username = document.getElementById("user").value;
	var userfree = freeUser(username);
	var password1 = document.getElementById("password").value;
	var password2 = document.getElementById("password2").value;
	var matchpw = matchPassword(password1, password2);
	var email = document.getElementById("email").value;
	confirmRegistration(userfree,matchpw, email);
	
}

function confirmRegistration(userfree, matchpw, email)
{
	if(userfree == false)
	{
		document.getElementById("response1").innerHTML("This username is already in use");
	}
	if(matchpw == false)
	{
		document.getElementById("responsepw").innerHTML("The passwords do not match");
	}
	if(email == "")
	{
		document.getElementById("response2").innerHTML("Please enter an email");
	}
	//Send data if everything is okay 
	if(userfree && matchpw && email != "")
	{
		var user = document.getElementById("user").value;
		var pw = document.getElementById("password").value;
		var key = Math.floor((Math.random() * 100) +1);
		var jsondata = {'username': user, 'password': pw, 'email': email, 'SharedKey': key};
		var	data = JSON.stringify(jsondata);
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "register.php", true);
		xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		xhr.setRequestHeader('Content-Length', data.length);
		xhr.setRequestHeader("Connection", "close");
		xhr.onreadystatechange = function () 
		{
			if (xhr.readyState == 4 && xhr.status == 200) 
			{					
				document.writeln(xhr.responseText);
			}
		};		
		xhr.send(data);
	}
}

function matchPassword(pw1, pw2)
{
	if(pw1==pw2)
		return true;
	else
		return false;
}

function freeUser(user)
{
	var jsondata ={'username':user};
	var response = false;
	var data = JSON.stringify(jsondata);
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "freeuser.php", true);
	xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.setRequestHeader('Content-Length', data.length);
    xhr.setRequestHeader("Connection", "close");
	xhr.onreadystatechange = function ()
	{
		if (xhr.readyState == 4 && xhr.status == 200) 
		{
        	jsondata = JSON.parse(xhr.responseText);
			if(jsondata == "true")
				response = true;
		}
	};
	xhr.send(data);
	return response;
}

window.addEventListener("load", start, false);