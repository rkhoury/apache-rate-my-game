<?php
$searchfield = $_POST['searchfield'];

// $servername = "sql5.freesqldatabase.com";
// $username = "sql597652";
// $password = "aW4%aI1!";
// $dbname = "sql597652";

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "RateMyGame";

$dbconn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (mysqli_connect_errno()) {
		echo "Failed to connect to Database: " . mysqli_connect_error(); }

$search_query = mysqli_query($dbconn,"SELECT * FROM Game WHERE name LIKE '%$searchfield%' ");



//if zero resuts
if(mysqli_num_rows($search_query)==0){
	echo <<<EOD
<!DOCTYPE html>
<html>
 <head>
<meta charset="utf-8">
        <title>Search DB</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="userLogin.js"></script>
<style>
    .logout {
    position: fixed;
    bottom: 2em;
    right: 0px;
    text-decoration: none;
    color: #000000;
    background-color: rgba(235, 235, 235, 0.80);
    font-size: 12px;
    padding: 1em;
    display: none;
}

.logout:hover {    
    background-color: rgba(135, 135, 135, 0.50);
}
</style>
</head>
	<body>
	   <div class="page">
        <header class="masthead" role="banner">
        <p class="logo"><a href="index.html"><img src="images/banner.png" width="95%"/></a></p>
        <nav role="navigation">
            <ul class="nav-main">
                <li><a href="index.html">Home<a></li>
                <li><a href="games.html">Games</a></li>
                <li><a href="members.html">Members</a></li>
                <li><a href="contact.html">Contact</a></li>
           <li><div class="search">
        	<form action="searchresult.php" method="post">
            	SEARCH: <input type="text" name="searchfield"/>
                <input id="submit" type="submit" name="submit" value="submit"/>
            </form>
        </div></li>
        </ul>
        </nav>
    </header>
	<main class="main">
    	<h1>No results. Please Refine your search and try again.</h1>

	 </main>
	 <footer class="footer">
        <a href="#" class="logout">Logout, <span id="loggedinuser"></span></a>
        <p>Copyright &copy; RateMyGame.com 2015</p>
    </footer>
    </div>
	    </body>
	</html> 
EOD;
//lists positive search results
}else{

	echo <<<EOD
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Rate My Game - Search</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
    <body>
  	<div class="page">
    <header class="masthead" role="banner">
        <p class="logo"><a href="index.html"><img src="images/banner.png" width="95%"/></a></p>
        <nav role="navigation">
            <ul class="nav-main">
                <li><a href="index.html">Home</a></li>
                <li><a href="games.html">Games</a></li>
                <li><a href="members.html">Members</a></li>
                <li><a href="contact.html">Contact</a></li>
           <li><div class="search">
        	<form action="searchresult.php" method="post">
            	SEARCH: <input type="text" name="searchfield"/>
                <input id="submit" type="submit" name="submit" value="submit"/>
            </form>
        </div></li>
        </ul>
        </nav>
    </header>
    <div class="contariner">
    <main class="main">
    <h1>Your Search was successful!</h1>
EOD;

	while ($row = mysqli_fetch_array($search_query)){
		
	    echo '<div class="game">
	    <h2><a href="loadGamePage.php?game='.$row['id_game'].'">'.$row['name'].'</a></h2>';
	 


	    echo 'Publisher: '.$row['pub_name'].'</a></b>';

	    //Check if rated or not, if rated print 
	    $id=$row['id_game'];
		$ratingcheck_query = mysqli_query($dbconn,"SELECT * FROM Rates WHERE id_game = '$id' ");
		if(mysqli_num_rows($ratingcheck_query)==0){
			echo '<br/> Average Rating: <b>Not Rated</b>';
		}else{
			echo '<br/> Average Rating: <b>'.$row['ave_rating'].'</b>';
		}
	    echo '<br/> Release Date: <b>'.$row['date'].'</b>';
	    echo '</div>';
    }
	
	echo '
</main>
</div>
<footer class="footer">
        <p>Copyright &copy; RateMyGame.com 2015</p>
    </footer>
</div>
</body>
</html>';

}
mysqli_close($dbcon);
?>