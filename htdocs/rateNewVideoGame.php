
// NOTES
// Assumes there is a master database called videogames
// Assumes there is table that contains the systems that we are currently allowing to input in the database
// Assumes there is a table called "videogames" in the database videogames that has the actual video games (lol)
// Assumes that we have passed the data to this php program through post
// Assumes that ratings for a video game are between 1 and 10

<?php

// retrieves the values from post, if that is how we are passing it around
$u = $_POST['username'];
$videogame = $_POST['videogame'];
$system = $_POST['system'];
$rating = $_POST['rating'];
$numberOfRatings = 0;

$servername = "localhost";
$username = "root";
$password = "";
// Assume that the dbname is videogames
$dbname = "videogames";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysql_error());
}

// Checks to see if this video game on this system already exists in our db
$query = "SELECT * FROM videogames WHERE videogame LIKE '" . $videogame . "'";
$result = mysqli_query($conn, $query);
if ($result ->num_rows < 1) 
{
echo <<<EOD
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Rate My Game</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="page">
    <header class="masthead" role="banner">
        <p class="logo"><a href="index.html"><img src="images/banner.png" width="95%"/></a></p>
        <nav role="navigation">
            <ul class="nav-main">
                <li><a href="index.html">Home<a></li>
                <li><a href="games.html">Games</a></li>
                <li><a href="members.html">Members</a></li>
                <li><a href="contact.html">Contact</a></li>
           <li><div class="search">
        	<form action="searchresult.php" method="post">
            	SEARCH: <input type="text" name="searchfield"/>
                <input id="submit" type="submit" name="submit" value="submit"/>
            </form>
        </div></li>
        </ul>
        </nav>
    </header>
    <div class="container">
    	<main class="main">
        <h1>Invalid Request!</h1>
        <p> This game is not in our database. Please contact one of the admins if you think there was a mistake.</p>
        </main>
    </div>
    <footer class="footer">
        <p>Copyright &copy; RateMyGame.com 2015</p>
    </footer>
</div>
</body>
</html>
EOD;
}

// This video game exists in our db
else
{
	// The rating is not in the valid range
	if ($rating < 1 || $rating > 10)
	{
echo <<<EOD
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Rate My Game</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="page">
    <header class="masthead" role="banner">
        <p class="logo"><a href="index.html"><img src="images/banner.png" width="95%"/></a></p>
        <nav role="navigation">
            <ul class="nav-main">
                <li><a href="index.html">Home<a></li>
                <li><a href="games.html">Games</a></li>
                <li><a href="members.html">Members</a></li>
                <li><a href="contact.html">Contact</a></li>
           <li><div class="search">
        	<form action="searchresult.php" method="post">
            	SEARCH: <input type="text" name="searchfield"/>
                <input id="submit" type="submit" name="submit" value="submit"/>
            </form>
        </div></li>
        </ul>
        </nav>
    </header>
    <div class="container">
    	<main class="main">
        <h1>Invalid Request!</h1>
        <p> Your rating is invalid. Please rate the game on a scale of 1 to 10.</p>
        </main>
    </div>
    <footer class="footer">
        <p>Copyright &copy; RateMyGame.com 2015</p>
    </footer>
</div>
</body>
EOD;

	}
	// first we try to find the system that this video game coordinates to
	if ($result ->num_rows > 1)
	{
		$sameSystem = False;
			while($row = mysqli_fetch_array($result))
			{
				if ($row["system"] == $system)
				{
					// We found the right video game on the right system
					$sameSystem = True;
					$currentRating = $row["rating"];
					$currentNumberOfRatings = $row["numberOfRatings"];
					$rowNumber = $row["id"];
					
					// If this is the first time the video game has been rated
					if ($currentNumberOfRatings == 0)
					{
						$currentNumberOfRatings = $currentNumberOfRatings + 1;
						$query = "UPDATE videogames SET rating=$rating WHERE id=$rowNumber";
						$result = mysqli_query($conn, $query);
						$query = "UPDATE videogames SET numberOfRatings=$currentNumberOfRatings WHERE id=$rowNumber";
						$result = mysqli_query($conn, $query);
					}
					// Otherwise other people have already rated this videogame
					else
					{
						$newRating = (($currentRating * $currentNumberOfRatings) +  $rating) / ($currentNumberOfRatings + 1);
						$currentNumberOfRatings = $currentNumberOfRatings + 1;
						$query = "UPDATE videogames SET rating=$newRating WHERE id=$rowNumber";
						$result = mysqli_query($conn, $query);
						$query = "UPDATE videogames SET numberOfRatings=$currentNumberOfRatings WHERE id=$rowNumber";
						$result = mysqli_query($conn, $query);
					}
				}
			}

		if ($sameSystem == False)
		{
echo <<<EOD
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Rate My Game</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="page">
    <header class="masthead" role="banner">
        <p class="logo"><a href="index.html"><img src="images/banner.png" width="95%"/></a></p>
        <nav role="navigation">
            <ul class="nav-main">
                <li><a href="index.html">Home<a></li>
                <li><a href="games.html">Games</a></li>
                <li><a href="members.html">Members</a></li>
                <li><a href="contact.html">Contact</a></li>
           <li><div class="search">
        	<form action="searchresult.php" method="post">
            	SEARCH: <input type="text" name="searchfield"/>
                <input id="submit" type="submit" name="submit" value="submit"/>
            </form>
        </div></li>
        </ul>
        </nav>
    </header>
    <div class="container">
    	<main class="main">
        <h1>Invalid Request!</h1>
        <p>The game exists in the system but the game system provided doesn't match. Please contact admin for help.</p>
        </main>
    </div>
    <footer class="footer">
        <p>Copyright &copy; RateMyGame.com 2015</p>
    </footer>
</div>
</body>
EOD;
		}
	}
}