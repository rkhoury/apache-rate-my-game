
// NOTES
// Assumes there is a master database called videogames
// Assumes there is table that contains the systems that we are currently allowing to input in the database
// Assumes there is a table called "videogames" in the database videogames that has the actual video games (lol)
// Assumes that we have passed the data to this php program through post
// Assumes that the table has an auto-incrementing field for id (called id)

// Sets the initial rating to 0 when entering a video game in to the database
// Sets the initial number of ratings for a particular video game to 0 when first entered


<?php

// retrieves the values from post, if that is how we are passing it around
$u = $_POST['username'];
$videogame = $_POST['videogame'];
$system = $_POST['system'];
$rating = 0;
$numberOfRatings = 0;

$servername = "localhost";
$username = "root";
$password = "";
// Assume that the dbname is videogames
$dbname = "videogames";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysql_error());
} 

// Checks to see if we allow this system in our db
$query = "SELECT * FROM systems WHERE system LIKE '" . $system . "'";
$result = mysqli_query($conn, $query);
if ($result ->num_rows != 1) 
{
echo <<<EOD
	<html>
	<head>
	<meta charset="utf-8">
    <title>Rate My Game - Error</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
	<div class="page">
        <header class="masthead" role="banner">
        <p class="logo"><a href="index.html"><img src="images/banner2.png" width="95%"/></a></p>
        <nav role="navigation">
            <ul class="nav-main">
                <li><a href="index.html">Home<a></li>
                <li><a href="games.html">Games</a></li>
                <li><a href="">Search</a></li>
                <li><a href="members.html">Members</a></li>
                <li><a href="contact.html">Contact</a></li>
           </ul>
        </nav>
    </header>
    <div class="container">
    	<main class="main">
		<h1> Sorry we don't allow this system in our database. Please contact the administrator.</h1>
		</main>
	</div>
	<footer class="footer">
		<p>Copyright &copy; RateMyGame.com 2015</p>
	</footer>
   	</div>
	</body>
	</html>
EOD;
}
// Else we accept this system in our database
else
{
	// We check to see if the video game already exists in our database
	$query = "SELECT * FROM videogames WHERE videogame LIKE '" . $videogame . "'";
	$result = mysqli_query($conn, $query);
	// This video game is not in the database so we add it
	if ($result ->num_rows != 1)
	{
		$query = "INSERT INTO videogames ( videogame, system, rating, numberOfRatings) VALUES ( $videogame, $system, $rating, $numberOfRatings)";
		$result = mysqli_query($conn, $query);
	}

	// Else this video game already exists in the database
	// But we check to see if the game already exists on this system (think of multiplatform games)
	else
	{
		$sameSystem = False;
			while($row = mysqli_fetch_array($result))
			{
				if ($row["system"] == $system)
				{
					$sameSystem = True;
				}
			}
			// The game is already in our database and exists on the same system
		if ($sameSystem)
		{
echo <<<EOD
	<head>
	<meta charset="utf-8">
    <title>Rate My Game - Error</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
	<div class="page">
        <header class="masthead" role="banner">
        <p class="logo"><a href="index.html"><img src="images/banner2.png" width="95%"/></a></p>
        <nav role="navigation">
            <ul class="nav-main">
                <li><a href="index.html">Home<a></li>
                <li><a href="games.html">Games</a></li>
                <li><a href="">Search</a></li>
                <li><a href="members.html">Members</a></li>
                <li><a href="contact.html">Contact</a></li>
           </ul>
        </nav>
    </header>
    <div class="container">
    	<main class="main">
					<h1> Sorry we already have this game in our database. Thanks for trying to add it though!</h1>
		</main>
	</div>
	<footer class="footer">
		<p>Copyright &copy; RateMyGame.com 2015</p>
	</footer>
   	</div>
	</body>
	</html>
EOD;
		}

		// This game is not in our database so we add it
		else 
		{
			$query = "INSERT INTO videogames (videogame, system, rating, numberOfRatings) VALUES ($videogame, $system, $rating, $numberOfRatings)";
			$result = mysqli_query($conn, $query);
		}

	}
}