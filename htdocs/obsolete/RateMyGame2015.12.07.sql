-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 08, 2015 at 11:43 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `RateMyGame`
--

-- --------------------------------------------------------

--
-- Table structure for table `Game`
--

CREATE TABLE `Game` (
  `id_game` char(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `artwork` varchar(200) DEFAULT NULL,
  `ave_rating` double DEFAULT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `pub_name` varchar(100) DEFAULT NULL,
  `pub_address` varchar(200) DEFAULT NULL,
  `profile_text` varchar(100) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Dumping data for table `Game`
--

INSERT INTO `Game` (`id_game`, `name`, `artwork`, `ave_rating`, `genre`, `date`, `pub_name`, `pub_address`, `profile_text`, `website`) VALUES
('mgs5g', 'Metal Gear Solid V: Ground Zeroes', 'mgs5g.png', 6, NULL, '2014-03-18', 'Konami', 'http://www.konami.com', NULL, NULL),
('mgs5p', 'Metal Gear Solid V: The Phantom Pain', 'mgs5p', NULL, NULL, '2015-09-01', 'Konami', 'http://www.konami.com', NULL, NULL),
('pong1', 'Pong', 'pong.jpg', NULL, NULL, '1972-11-29', 'Atari Inc.', 'http://atari.com/', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Game_Add_Request`
--

CREATE TABLE `Game_Add_Request` (
  `id_change` int(5) NOT NULL,
  `artwork` varchar(200) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `profile_text` varchar(100) DEFAULT NULL,
  `platform` varchar(100) DEFAULT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `change_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `admin_comment` varchar(100) DEFAULT NULL,
  `admin_date` date DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `admin_username` varchar(100) NOT NULL,
  `pub_name` varchar(100) DEFAULT NULL,
  `pub_address` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `Game_Add_Request`:
--   `username`
--       `User` -> `username`
--   `admin_username`
--       `User` -> `username`
--

-- --------------------------------------------------------

--
-- Table structure for table `Media`
--

CREATE TABLE `Media` (
  `id_game` char(5) NOT NULL,
  `medialocation` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `Media`:
--   `id_game`
--       `Game` -> `id_game`
--

-- --------------------------------------------------------

--
-- Table structure for table `Platform`
--

CREATE TABLE `Platform` (
  `system` varchar(30) NOT NULL,
  `id_game` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `Platform`:
--   `id_game`
--       `Game` -> `id_game`
--

-- --------------------------------------------------------

--
-- Table structure for table `Rates`
--

CREATE TABLE `Rates` (
  `rating` double DEFAULT NULL,
  `body` varchar(1500) DEFAULT NULL,
  `time` time DEFAULT NULL,
  `date` date DEFAULT NULL,
  `username` varchar(100) NOT NULL DEFAULT '',
  `id_game` char(5) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `Rates`:
--   `username`
--       `User` -> `username`
--   `id_game`
--       `Game` -> `id_game`
--

--
-- Dumping data for table `Rates`
--

INSERT INTO `Rates` (`rating`, `body`, `time`, `date`, `username`, `id_game`) VALUES
(6, 'This is an awesome game! I enjoyed it a lot. So much fun. Much fun much fun. ', '05:22:00', '2015-12-02', 'Blue', 'mgs5g');

--
-- Triggers `Rates`
--
DELIMITER $$
CREATE TRIGGER `Ave_Del_Recalc` AFTER DELETE ON `rates`
 FOR EACH ROW UPDATE Game g 
SET g.ave_rating = ( SELECT avg(rating) 
	FROM Rates rat 
	WHERE rat.id_game = OLD.id_game ) 
WHERE g.id_game = OLD.id_game
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Ave_Ins_Recalc` AFTER INSERT ON `rates`
 FOR EACH ROW UPDATE Game g 
SET g.ave_rating = ( SELECT avg(rating) 
	FROM Rates rat 
	WHERE rat.id_game = NEW.id_game ) 
WHERE g.id_game = NEW.id_game
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `Ave_Upd_Recalc` AFTER UPDATE ON `rates`
 FOR EACH ROW UPDATE Game g 
SET g.ave_rating = ( SELECT avg(rating) 
	FROM Rates rat 
	WHERE rat.id_game = NEW.id_game ) 
WHERE g.id_game = NEW.id_game
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Sessions`
--

CREATE TABLE `Sessions` (
  `username` varchar(100) NOT NULL,
  `sessionID` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `Sessions`:
--   `username`
--       `User` -> `username`
--

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `SharedKey` int(2) NOT NULL,
  `password` varchar(200) NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS FOR TABLE `User`:
--

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`username`, `email`, `SharedKey`, `password`, `admin`) VALUES
('Blue', '', 0, 'Hour', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Game`
--
ALTER TABLE `Game`
  ADD PRIMARY KEY (`id_game`);
  

--
-- Indexes for table `Game_Add_Request`
--
ALTER TABLE `Game_Add_Request`
  ADD PRIMARY KEY (`id_change`),
  ADD KEY `username` (`username`),
  ADD KEY `admin_username` (`admin_username`);

--
-- Indexes for table `Manages_List`
--
-- ALTER TABLE `Manages_List`
--   ADD PRIMARY KEY (`username`,`type`),
--   ADD KEY `id_game` (`id_game`);

--
-- Indexes for table `Media`
--
ALTER TABLE `Media`
  ADD KEY `id_game` (`id_game`);

--
-- Indexes for table `Platform`
--
ALTER TABLE `Platform`
  ADD PRIMARY KEY (`id_game`,`system`);



--
-- Indexes for table `Rates`
--
ALTER TABLE `Rates`
  ADD PRIMARY KEY (`id_game`,`username`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `Sessions`
--
ALTER TABLE `Sessions`
  ADD PRIMARY KEY (`sessionID`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Game_Add_Request`
--
ALTER TABLE `Game_Add_Request`
  MODIFY `id_change` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Sessions`
--
ALTER TABLE `Sessions`
  MODIFY `sessionID` int(6) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--



--
-- Constraints for table `Game_Add_Request`
--
ALTER TABLE `Game_Add_Request`
  ADD CONSTRAINT `Game_Add_Request_ibfk_1` FOREIGN KEY (`username`) REFERENCES `User` (`username`),
  ADD CONSTRAINT `Game_Add_Request_ibfk_2` FOREIGN KEY (`admin_username`) REFERENCES `User` (`username`);

--
-- Constraints for table `Manages_List`
--
-- ALTER TABLE `Manages_List`
--   ADD CONSTRAINT `Manages_List_ibfk_1` FOREIGN KEY (`username`) REFERENCES `User` (`username`),
--   ADD CONSTRAINT `Manages_List_ibfk_2` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id_game`);

--
-- Constraints for table `Media`
--
ALTER TABLE `Media`
  ADD CONSTRAINT `Media_ibfk_1` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id_game`);

--
-- Constraints for table `Platform`
--
ALTER TABLE `Platform`
  ADD CONSTRAINT `Platform_ibfk_1` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id_game`);

--
-- Constraints for table `Rates`
--
ALTER TABLE `Rates`
  ADD CONSTRAINT `Rates_ibfk_1` FOREIGN KEY (`username`) REFERENCES `User` (`username`),
  ADD CONSTRAINT `Rates_ibfk_2` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id_game`);

--
-- Constraints for table `Sessions`
--
ALTER TABLE `Sessions`
  ADD CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`username`) REFERENCES `User` (`username`);

ALTER TABLE Game
ADD CONSTRAINT genre_constraint
CHECK (genre IN('RPG', 'Action', 'Simulation','Adventure','Sports','MMO','Strategy'))

ALTER TABLE Platform
ADD CONSTRAINT platform_constraint
CHECK (system IN('PS3', 'PS4', 'XBoxOne','Xbox360','PC','Mac','Web','Wii','Mobile'))


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
