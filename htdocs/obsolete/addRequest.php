	
<?php
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "RateMyGame";

$dbconn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (mysqli_connect_errno()) {
		echo "Failed to connect to Database: " . mysqli_connect_error(); }




echo <<<EOD
<!DOCTYPE html>
<html>
 <head>
<meta charset="utf-8">
<title>Rate My Game - Game Add Request</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="page">
<header class="masthead" role="banner">
        <p class="logo"><a href="index.html"><img src="images/banner.png" width="95%"/></a></p>
        <nav role="navigation">
            <ul class="nav-main">
                <li><a href="index.html">Home<a></li>
                <li><a href="games.html">Games</a></li>
                <li><a href="members.html">Members</a></li>
                <li><a href="contact.html">Contact</a></li>
           <li><div class="search">
        	<form action="searchresult.php" method="post">
            	SEARCH: <input type="text" name="searchfield"/>
                <input id="submit" type="submit" name="submit" value="submit"/>
            </form>
        </div></li>
        </ul>
        </nav>
    </header>
    <div class="container">
        <main class="main-aside">
        	<h1>Game Add Request</h1>
            <p>If you would like to rate a game that is not currently available on our website, you may request to have it added using the form below.</p>
          
            <p></p>
        </main>
        
			<br><br><br><br><br><br><br><br><br>
	        <form action="submitRequest.php" method="post" id="addrequestform">
	        	
	        	 Game Name:
				  <input type="text" name="name"><br>

				 Publisher Name:
				  <input type="text" name="p_name"><br>

				  Publisher Website:
				  <input type="text" name="p_website"><br>

				
				
				Artwork Link:
				<input type="text" name="artwork"><br>
				
				Genre: 
				  	<select name="genre" form="addrequestform">
					  <option value="blank">Please Select</option>
					  <option value="Action">Action</option>
					  <option value="Adventure">Adventure</option>
					  <option value="MMO">MMO</option>
					  <option value="RPG">RPG</option>
					  <option value="Simulation">Simulation</option>
					  <option value="Sports">Sports</option>
					  <option value="Strategy">Strategy</option>
					</select><br>
				  Release date:
				  <input type="date" name="releasedate"><br>

				  	System:
			<select name="system1" form="addrequestform" onchange="newPlat1(this.value)">
				  <option value="blank">Please Select</option>
				  <option value="PS3">PS3</option>
				  <option value="PS4">PS4</option>
				  <option value="XBoxOne">XBoxOne</option>
				  <option value="PC">PC</option>
				  <option value="Mac">Mac</option>
				   <option value="Mobile">Mobile</option>
				  <option value="Web">Web</option>
				  <option value="Wii">Wii</option>
			</select>

			<p id="newPlat1"></p>
			<p id="newPlat2"></p>
				  <br><br>
				  <input type="submit" value="Submit">
			
			

<script type = "text/javascript">


function newPlat1(val){
	if (val != "blank"){
		document.getElementById('newPlat1').innerHTML = 'Additional Systems: <select name="system2" form="addrequestform" onchange="newPlat2(this.value)"><option value="">Select</option><option value="PS3">PS3</option><option value="PS4">PS4</option><option value="XBoxOne">XBoxOne</option><option value="PC">PC</option><option value="Mac">Mac</option><option value="Mobile">Mobile</option><option value="Web">Web</option><option value="Wii">Wii</option></select>';}

	else {document.getElementById('newPlat1').innerHTML ="";}
}

function newPlat2(val){
	if (val != ""){
		document.getElementById('newPlat2').innerHTML = '<select name="system3" form="addrequestform"><option value="">Select</option><option value="PS3">PS3</option><option value="PS4">PS4</option><option value="XBoxOne">XBoxOne</option><option value="PC">PC</option><option value="Mac">Mac</option><option value="Mobile">Mobile</option><option value="Web">Web</option><option value="Wii">Wii</option></select>';}

	else {document.getElementById('newPlat2').innerHTML ="";}
}

</script>
			</form>

			Please note that if your form is incomplete or incorrect your submission may be rejected.
    </div>
    <footer class="footer">
        <p>Copyright &copy; RateMyGame.com 2015</p>
    </footer>
</div>
</body>
</html>
EOD;

mysqli_close($dbcon);
?>
