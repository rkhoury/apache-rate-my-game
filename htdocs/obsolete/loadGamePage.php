<?php
$game = $_GET['game'];
$sessionid = $_COOKIE['sessionid'];

if ($game == "") header("Location:games.html");

$servername = "localhost";
$username = "root";
$password = "root";
// Assume that the dbname is videogames
$dbname = "RateMyGame";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysql_error());
}

if ($sessionid != "")
{
  $query = "SELECT * FROM sessions WHERE sessionID LIKE '" . $sessionid . "'";
  $result = mysqli_query($conn, $query);
  if ($result ->num_rows < 1) 
    {
    }
    else
{
  $row = mysqli_fetch_array($result);
  $currentloggedinuser = $row["username"];
}
}


$game_query = "SELECT * FROM Game WHERE id_game = '" . $game . "'";
$game_result = mysqli_query($conn, $game_query);
$game_row = mysqli_fetch_array($game_result);

$game_name = $game_row["name"];
if ($game_name == "") header("Location:games.html");
$rating = $game_row["ave_rating"];

if ($rating == "") $rating = "Not yet Rated. Be the first to rate this game!";

$pub_name = $game_row["pub_name"];
$artwork = $game_row["artwork"];
$genre = $game_row["genre"];
$rel_date = $game_row["date"];
$profile = $game_row["profile_text"];
$website = $game_row["website"];


$system2 = "";
$system3 = "";
$count = 0;



$platform_result = mysqli_query($conn, "SELECT * FROM Platform WHERE id_game = '" . $game . "'");
while ($plat_row = mysqli_fetch_array($platform_result)){
    if($count == 0) $system1 = $plat_row["system"];
    if($count == 1) $system2 = $plat_row["system"];
    if($count == 2) $system3 = $plat_row["system"];
    $count = $count + 1;
}

$platforms = $system1." ".$system2." ".$system3;


$comment_rate_query = mysqli_query($conn, "SELECT * FROM Rates WHERE id_game = '" . $game . "' ORDER BY date DESC, time DESC");





echo <<<EOD
<!DOCTYPE html>
<html>
 <head>
<meta charset="utf-8">
<title>$game_name</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="userLogin.js"></script>
<script>
    function getusername(){
        var current_user = document.getElementById("loggedinuser").innerHTML;
        return current_user;
        
        
}
</script>
            <script>
            function addusername(){
            var user = document.getElementById("loggedinuser").innerHTML;
            (document.getElementById("userid")).value = user;
            }
            </script>

</head>

<body>
<div class="page">
    <header class="masthead" role="banner">
        <p class="logo"><a href="index.html"><img src="images/banner.png" width="95%"/></a></p>
        <nav role="navigation">
            <ul class="nav-main">
                <li><a href="index.html">Home<a></li>
                <li><a href="games.html">Games</a></li>
                <li><a href="members.html">Members</a></li>
                <li><a href="contact.html">Contact</a></li>
           <li><div class="search">
        	<form action="searchresult.php" method="post">
            	SEARCH: <input type="text" name="searchfield"/>
                <input id="submit" type="submit" name="submit" value="submit"/>
            </form>
        </div></li>
        </ul>
        </nav>
      </header>
      <div class="container">
      	<main class="main">
	<div id="gameCont">
        	<h1>$game_name</h1>
	<div id="gameImg">
            <img src="$artwork" alt="$game_name"><br>
            <a href="$website">Official Game Homepage</a></div>
            <p><font>Publisher:</font> <b>$pub_name</b></p>
            <p><font>Genre:</font> <b>$genre</b></p>
            <p><font>Release Date:</font><b> $rel_date</b></p>
            <p><font>System Platform(s):</font> <b>$platforms </b></p>
            <p><font>Average Rating:</font> <b>$rating</b> / 10 </p>
            <p><font>Profile:</font> $profile</p>
		<div id="gameImg"><a href="#formClass"><font><b>Rate this game now!</b></font></a></div>
	</div>
	<hr>
           <h2>User Ratings & Comments:</h2>

            
EOD;

while ($comment_rate_row = mysqli_fetch_array($comment_rate_query)){
    // $comment_rate_row["rating"];
    $comment_user = $comment_rate_row["username"];
    // $comment_rate_row["date"];
    // $comment_rate_row["time"];
   
    // $comment_rate_row["rating"];
    
    echo '<div class="game">
            On '.$comment_rate_row["date"].' at '.$comment_rate_row["time"].' 
            <b>'.$comment_rate_row["username"].'</b> rated this game with a score of: '.$comment_rate_row["rating"].'
            <p></p>';


    echo'
           






           

                "'.$comment_rate_row["body"].'"';
               if ($comment_rate_row["username"] == $currentloggedinuser)
            {
              echo '\'=<form action="deletePost.php" method="post" id="deletepost"> <input 

id="userid" type="hidden" name="user" value="'.$comment_user.'" ><input id="gameid" type="hidden" name="game" value="'.$game.'" ><input type="submit" 

value="Delete Comment" id="submit"></form>';
            }
            echo '</div>';
}

if ($currentloggedinuser == "")
{
  echo <<<EOD
          <hr>
  <div id="formClass">
    <h2>Rate This Game</h2>
            <p>Please log in to Rate This Game!</p>
EOD;
}
else
{
echo <<<EOD
          <hr>
	<div id="formClass">
		<h2>Rate This Game</h2>
            <p>If you have not yet rated and commented this game, you may do so below.</p>
            <p>Please note that if you already rated the game and rate it again, your rating will be overwritten.</p>
            
            

            <form action="commentRate.php" method="post" onsubmit="addusername()" id="commentRate">
                
                 <p>Rating:
                 <select name="rating" form="commentRate">
                      <option value="10">10</option>
                      <option value="9">9</option>
                      <option value="8">8</option>
                      <option value="7">7</option>
                      <option value="6">6</option>
                      <option value="5">5</option>
                      <option value="4">4</option>
                      <option value="3">3</option>
                      <option value="2">2</option>
                      <option value="1">1</option>
                  </select></p>
		<p>
                  <textarea id="comment" name="comment" rows="5" cols="100"></textarea>
                  <input id="userid" type="hidden" name="user" >
                  <input id="id_game" type="hidden" name="id_game" value="$game" ></p>
                  <p><input type="submit" value="Submit" id="submit"></p>
            </form>
EOD;
}
echo <<<EOD

	</div>
  
        </main>


     </div>
    <footer class="footer">
        <a href="#" class="logout">Logout, <span id="loggedinuser"></span></a>
        <a href="#" class="admin">Admin Panel</a>
        <p>Copyright &copy; RateMyGame.com 2015</p>
    </footer>
</div>
</body>
</html>
EOD;

mysqli_close($conn);
?>