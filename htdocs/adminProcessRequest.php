<?php

$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "RateMyGame";

$dbconn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (mysqli_connect_errno()) {
        echo "Failed to connect to Database: " . mysqli_connect_error(); }


$id_change = $_POST["id_change"];
$title = $_POST["name"];
$website = $_POST["website"];
$pub_name = $_POST["pub_name"];
$artwork = $_POST["artwork"];
$genre = $_POST["genre"];
$rel_date = $_POST["rel_date"];
$profile_text = $_POST["profile_text"];
$id_game = $_POST["id_game"];

$platform =$_POST["platform"];
$platform2 =$_POST["Platform2"];
$platform3 =$_POST["Platform3"];

$status = $_POST["decision"];
$admin_comment = $_POST["admin_comment"];
$admin = $_POST["admin"];



$get_add_request = mysqli_query($dbconn, "SELECT * FROM Game_Add_Request WHERE id_change = '" . $id_change . "'");
$add_request_row = mysqli_fetch_array($get_add_request);

$user = $add_request_row["username"];


if($status=="Approved"){

$change_request_approved = mysqli_query($dbconn, "UPDATE Game_Add_Request SET status='".$status."',admin_comment = '" . $admin_comment . "', admin_date=now(), admin='" . $admin . "', title = '" . $title . "', artwork = '" . $artwork . "', website = '" . $website . "', profile_text = '" . $profile_text . "',platform = '" . $platform . "',genre = '" . $genre . "',pub_name = '" . $pub_name . "',Platform2 = '" . $Plaftorm2 . "',Platform3 = '" . $Platform3 . "', rel_date = '" . $rel_date . "'
WHERE id_change = '" . $id_change . "'");

$add_game_db = mysqli_query($dbconn, "Insert Into Game (id_game, name, artwork, genre, date, pub_name, profile_text, website) Values ('" . $id_game . "','" . $title . "','" . $artwork . "','" . $genre . "','" . $rel_date . "','" . $pub_name . "','" . $profile_text . "','" . $website . "');");

$insert_platform = mysqli_query($dbconn, "Insert Into Platform (system, id_game) Values ('" . $platform . "','" . $id_game . "');");

if($platform2!=""){
$insert_platform = mysqli_query($dbconn, "Insert Into Platform (system, id_game) Values ('" . $Platform2 . "','" . $id_game . "');");
}

if($platform3!=""){
	$insert_platform = mysqli_query($dbconn, "Insert Into Platform (system, id_game) Values ('" . $Platform3 . "','" . $id_game . "');");
}


echo <<<EOD
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title>Rate My Game - Admin Approval Form</title>
  <link href="css/style.css" rel="stylesheet" type="text/css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="userLogin.js"></script>



<style>
    .logout {
    position: fixed;
    bottom: 2em;
    right: 0px;
    text-decoration: none;
    color: #000000;
    background-color: rgba(235, 235, 235, 0.80);
    font-size: 12px;
    padding: 1em;
    display: none;
}

.logout:hover {    
    background-color: rgba(135, 135, 135, 0.50);
}
</style>
 </head>
<body>
<div class="page">
  <header class="masthead" role="banner">
        <p class="logo"><a href="index.html"><img src="images/banner2.png" width="95%"/></a></p>
        <nav role="navigation">
            <ul class="nav-main">
                <li><a href="index.html">Home<a></li>
                <li><a href="games.html">Games</a></li>
                <li><a href="members.html">Members</a></li>
                <li><a href="contact.html">Contact</a></li>
           <li><div class="search">
          <form action="searchresult.php" method="post">
              SEARCH: <input type="text" name="searchfield"/>
                <input id="submit" type="submit" name="submit" value="submit"/>
            </form>
        </div></li>
        </ul>
        </nav>
    </header>
    <div class="container">
    <main class="main">
      <h1>Add Request APPROVED. 
            <p><input type="hidden" id="sessionid" name="sessionid" type="text" value=""></p>
            Click below to enter your member profile.<br>
             <input id ="gotoadminarea" type="button" value="Admin Profile" onclick="window.location.href='memberhomepage.html'" />



</main>
</div>
    <footer class="footer">
        <a href="#" class="logout">Logout, <span id="loggedinuser"></span></a>
        <p>Copyright &copy; RateMyGame.com 2015</p>
    </footer>
</div>
</body>
</html>
 
EOD;


}else{

$change_request_rejected = mysqli_query($dbconn, "UPDATE Game_Add_Request SET status='".$status."',admin_comment = '" . $admin_comment . "', admin_date=now(), admin='" . $admin . "'
WHERE id_change = '" . $id_change . "'");



echo <<<EOD
<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">
  <title>Rate My Game - Admin Approval Form</title>
  <link href="css/style.css" rel="stylesheet" type="text/css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="userLogin.js"></script>



<style>
    .logout {
    position: fixed;
    bottom: 2em;
    right: 0px;
    text-decoration: none;
    color: #000000;
    background-color: rgba(235, 235, 235, 0.80);
    font-size: 12px;
    padding: 1em;
    display: none;
}

.logout:hover {    
    background-color: rgba(135, 135, 135, 0.50);
}
</style>
 </head>
<body>
<div class="page">
  <header class="masthead" role="banner">
        <p class="logo"><a href="index.html"><img src="images/banner2.png" width="95%"/></a></p>
        <nav role="navigation">
            <ul class="nav-main">
                <li><a href="index.html">Home<a></li>
                <li><a href="games.html">Games</a></li>
                <li><a href="members.html">Members</a></li>
                <li><a href="contact.html">Contact</a></li>
           <li><div class="search">
          <form action="searchresult.php" method="post">
              SEARCH: <input type="text" name="searchfield"/>
                <input id="submit" type="submit" name="submit" value="submit"/>
            </form>
        </div></li>
        </ul>
        </nav>
    </header>
    <div class="container">
    <main class="main">
      <h1>Add Request REJECTED. 
            <p><input type="hidden" id="sessionid" name="sessionid" type="text" value=""></p>
            Click below to enter your member profile.<br>
             <input id ="gotoadminarea" type="button" value="Admin Profile" onclick="window.location.href='memberhomepage.html'" />



</main>
</div>
    <footer class="footer">
        <a href="#" class="logout">Logout, <span id="loggedinuser"></span></a>
        <p>Copyright &copy; RateMyGame.com 2015</p>
    </footer>
</div>
</body>
</html>
 
EOD;




}



mysqli_close($dbcon);

?>
