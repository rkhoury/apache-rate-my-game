<?php
function check_Member($member)
{
	$server = 'localhost';
	$user = 'root';
	$pw = '';
	$database = 'ratemygame';
	$found = true; //no username exists

	$db= new mysqli($server, $user, $pass, $database);
	if ($db->connect_error) 
	{
	    die("Connection failed: " . $db->connect_error);
	}
	
	//find username in database if it exists
	$query = sprintf("SELECT * FROM members WHERE username = '%s'",
    					mysql_real_escape_string($member));
	if ($result = $db->query($query)) 
	{ 
        while($obj = $result->fetch_object())
		{ 
        	$found = false;     
	    } 
	 } 
	 $result->close();
	  
	 //return the result   
	 return $found;
};	

require '/vendor/autoload.php';
$app = new \Slim\Slim(); 

$app->post('/', function () use ($app)
{
			
		//get and decode Json string
		$body = $app->request->getBody();
		$input = json_decode($body, true);
						
		//find the member and shared key from database
		$member = (string)$input["username"];
		$response = check_Member($member);
		$app->response->headers->set('Content-type', 'application/json');
		$app->response()->setStatus(200);
		$app->response->setBody(json_encode($response)); 

});
//run application
$app->run();	
?>