<?php
	function db_Connect()
	{
		$server = 'localhost';
		$user = 'root';
		$pw = '';
		$database = 'ratemygame';
		
		$db = new mysqli($server, $user, $pw, $database);
		if ($db->connect_error) {
	    	die("Connection failed: " . $db->connect_error);
		}
		return $db;
	};
	
	function add_Member($member, $password, $key, $email)
	{
		$db = db_Connect();
		$query = sprintf("INSERT INTO user ('username', 'email', 'SharedKey', 'password', 'admin')" .
							"VALUES ('%s', '%s', '$key', '%s', false);",
							mysql_real_escape_string($member),
							mysql_real_escape_string($email),
							mysql_real_escape_string($password));
		if($db->query($query)==TRUE)
			echo "New Record Created";
		else
			echo "An error occured";
	}
	
session_start();	
require '/vendor/autoload.php';
$app = new \Slim\Slim(); 

$app->post('/', function () use ($app){
			
		//get and decode Json string
		$body = $app->request->getBody();
		$input = json_decode($body, true);
						
		//find the member and shared key from database
		$member = (string)$input["username"];
		$password = (string)$input["password"];
		$key = (int)$input["SharedKey"];
		$email = (string)$input["email"];
		add_Member($member, $password, $key, $email);
});
//run application
$app->run();	
?>