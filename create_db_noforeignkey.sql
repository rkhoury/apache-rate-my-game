CREATE DATABASE RateMyGameDB

Create Table User (username Varchar(100) Not Null,email Varchar(100) Not Null, SharedKey int(2) NOT NULL, password varbinary(200) Not Null, admin boolean Not Null,  Primary Key (username))

Create Table Sessions ( username varchar(3) NOT NULL, SessionID int(6) NOT NULL,Primary Key (username))

Create Table Publisher (id_pub char(5) Not Null, name Varchar(100), website Varchar(100), logopic Varchar(200),  profile_text Varchar(1000), Primary Key (id_pub))

Create Table Game (id_game Char(5) Not Null, name Varchar(100) Not Null, artwork Varchar(200), ave_rating Real, platform Varchar(100), genre Varchar(100), id_pub Char(5), date Date, Primary Key (id_game))

Create Table Media (id_game Char(5) Not Null, medialocation varchar(300) Not Null)

Create Table Rates (username Varchar(100) Not Null, id_game Char(5) Not Null, rating Real, Primary Key (username,id_game))

Create Table Write_Comment (id_comment Char(5) Not Null, body Varchar(1500), time Time, date Date, username Varchar(100), id_game Char(5), Primary Key (id_comment))

Create Table Manages_List (username Varchar(100) Not Null, type Varchar(100) Not Null CHECK (type IN('Wishlist', 'Collection')), id_game Char(5) Not Null, Primary Key (username, type))

Create Table Game_Add_Request (id_change char(5) Not Null, id_pub char(5),artwork Varchar(200), website Varchar(100), profile_text Varchar(100), platform Varchar(100), genre Varchar(100), change_date Date, status Varchar(100) CHECK (status IN('Pending', 'Rejected', 'Approved')), admin_comment Varchar(100), admin_date Date, username Varchar(100) Not Null, admin_username Varchar(100) Not Null, Primary Key (id_change))


Create Table Publisher_Add_Request (id_change char(5) Not Null, id_pub char(5), name Varchar(100), website Varchar(100), logopic Varchar(200),  profile_text Varchar(100), change_date Date, status Varchar(100) CHECK (status IN('Pending', 'Rejected', 'Approved')), admin_comment Varchar(100), admin_date Date, username Varchar(100) Not Null, admin_username Varchar(100) Not Null, Primary Key (id_change))

