-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Dec 02, 2015 at 07:33 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `RateMyGame`
--

-- --------------------------------------------------------

--
-- Table structure for table `Game`
--

CREATE TABLE `Game` (
  `id_game` char(5) NOT NULL,
  `name` varchar(100) NOT NULL,
  `artwork` varchar(200) DEFAULT NULL,
  `ave_rating` double DEFAULT NULL,
  `platform` varchar(100) DEFAULT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `id_pub` char(5) DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Game`
--

INSERT INTO `Game` (`id_game`, `name`, `artwork`, `ave_rating`, `platform`, `genre`, `id_pub`, `date`) VALUES
('mgs5g', 'Metal Gear Solid V: Ground Zeroes', 'mgs5g.png', 6, NULL, NULL, 'konam', '2014-03-18'),
('mgs5p', 'Metal Gear Solid V: The Phantom Pain', 'mgs5p', NULL, NULL, NULL, 'konam', '2015-09-01'),
('pong1', 'Pong', 'pong.jpg', NULL, NULL, NULL, 'atari', '1972-11-29');

-- --------------------------------------------------------

--
-- Table structure for table `Game_Add_Request`
--

CREATE TABLE `Game_Add_Request` (
  `id_change` char(5) NOT NULL,
  `id_pub` char(5) DEFAULT NULL,
  `artwork` varchar(200) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `profile_text` varchar(100) DEFAULT NULL,
  `platform` varchar(100) DEFAULT NULL,
  `genre` varchar(100) DEFAULT NULL,
  `change_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `admin_comment` varchar(100) DEFAULT NULL,
  `admin_date` date DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `admin_username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Manages_List`
--

CREATE TABLE `Manages_List` (
  `username` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `id_game` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Media`
--

CREATE TABLE `Media` (
  `id_game` char(5) NOT NULL,
  `medialocation` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Platform`
--

CREATE TABLE `Platform` (
  `system` varchar(30) NOT NULL,
  `id_game` char(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Publisher`
--

CREATE TABLE `Publisher` (
  `id_pub` char(5) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `logopic` varchar(200) DEFAULT NULL,
  `profile_text` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Publisher`
--

INSERT INTO `Publisher` (`id_pub`, `name`, `website`, `logopic`, `profile_text`) VALUES
('atari', 'Atari Inc.', 'https://atari.com', 'atari.png', 'The original Atari, Inc. founded in 1972 by Nolan Bushnell and Ted Dabney was a pioneer in arcade games, home video game consoles, and home computers. The company''s products, such as Pong and the Atari 2600, helped define the electronic entertainment industry from the 1970s to the mid-1980s.'),
('konam', 'Konami', 'http://www.konami.com', 'konami.png', 'Konami is famous for popular video game series such as Castlevania, Contra, Dance Dance Revolution, Gradius, Frogger, Suikoden, Ganbare Goemon, Metal Gear, Mirmo, Beatmania, Pro Evolution Soccer, Silent Hill and Yu-Gi-Oh!');

-- --------------------------------------------------------

--
-- Table structure for table `Publisher_Add_Request`
--

CREATE TABLE `Publisher_Add_Request` (
  `id_change` char(5) NOT NULL,
  `id_pub` char(5) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `logopic` varchar(200) DEFAULT NULL,
  `profile_text` varchar(100) DEFAULT NULL,
  `change_date` date DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `admin_comment` varchar(100) DEFAULT NULL,
  `admin_date` date DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `admin_username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Rates`
--

CREATE TABLE `Rates` (
  `rating` double DEFAULT NULL,
  `body` varchar(1500) DEFAULT NULL,
  `time` time DEFAULT NULL,
  `date` date DEFAULT NULL,
  `username` varchar(100) NOT NULL DEFAULT '',
  `id_game` char(5) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Rates`
--

INSERT INTO `Rates` (`rating`, `body`, `time`, `date`, `username`, `id_game`) VALUES
(6, 'This is an awesome game! I enjoyed it a lot. So much fun. Much fun much fun. ', '05:22:00', '2015-12-02', 'Blue', 'mgs5g');

--
-- Triggers `Rates`
--


-- --------------------------------------------------------

--
-- Table structure for table `Sessions`
--

CREATE TABLE `Sessions` (
  `username` varchar(100) NOT NULL,
  `sessionID` int(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Sessions`
--

INSERT INTO `Sessions` (`username`, `sessionID`) VALUES
('Blue', 1);

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `SharedKey` int(2) NOT NULL,
  `password` varchar(200) NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`username`, `email`, `SharedKey`, `password`, `admin`) VALUES
('Blue', '', 0, 'Hour', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Game`
--
ALTER TABLE `Game`
  ADD PRIMARY KEY (`id_game`),
  ADD KEY `id_pub` (`id_pub`);

--
-- Indexes for table `Game_Add_Request`
--
ALTER TABLE `Game_Add_Request`
  ADD PRIMARY KEY (`id_change`),
  ADD KEY `username` (`username`),
  ADD KEY `admin_username` (`admin_username`);

--
-- Indexes for table `Manages_List`
--
ALTER TABLE `Manages_List`
  ADD PRIMARY KEY (`username`,`type`),
  ADD KEY `id_game` (`id_game`);

--
-- Indexes for table `Media`
--
ALTER TABLE `Media`
  ADD KEY `id_game` (`id_game`);

--
-- Indexes for table `Platform`
--
ALTER TABLE `Platform`
  ADD PRIMARY KEY (`id_game`,`system`);

--
-- Indexes for table `Publisher`
--
ALTER TABLE `Publisher`
  ADD PRIMARY KEY (`id_pub`);

--
-- Indexes for table `Publisher_Add_Request`
--
ALTER TABLE `Publisher_Add_Request`
  ADD PRIMARY KEY (`id_change`),
  ADD KEY `username` (`username`),
  ADD KEY `admin_username` (`admin_username`);

--
-- Indexes for table `Rates`
--
ALTER TABLE `Rates`
  ADD PRIMARY KEY (`id_game`,`username`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `Sessions`
--
ALTER TABLE `Sessions`
  ADD PRIMARY KEY (`sessionID`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Sessions`
--
ALTER TABLE `Sessions`
  MODIFY `sessionID` int(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Game`
--
ALTER TABLE `Game`
  ADD CONSTRAINT `Game_ibfk_1` FOREIGN KEY (`id_pub`) REFERENCES `Publisher` (`id_pub`);

--
-- Constraints for table `Game_Add_Request`
--
ALTER TABLE `Game_Add_Request`
  ADD CONSTRAINT `Game_Add_Request_ibfk_1` FOREIGN KEY (`username`) REFERENCES `User` (`username`),
  ADD CONSTRAINT `Game_Add_Request_ibfk_2` FOREIGN KEY (`admin_username`) REFERENCES `User` (`username`);

--
-- Constraints for table `Manages_List`
--
ALTER TABLE `Manages_List`
  ADD CONSTRAINT `Manages_List_ibfk_1` FOREIGN KEY (`username`) REFERENCES `User` (`username`),
  ADD CONSTRAINT `Manages_List_ibfk_2` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id_game`);

--
-- Constraints for table `Media`
--
ALTER TABLE `Media`
  ADD CONSTRAINT `Media_ibfk_1` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id_game`);

--
-- Constraints for table `Platform`
--
ALTER TABLE `Platform`
  ADD CONSTRAINT `Platform_ibfk_1` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id_game`);

--
-- Constraints for table `Publisher_Add_Request`
--
ALTER TABLE `Publisher_Add_Request`
  ADD CONSTRAINT `Publisher_Add_Request_ibfk_1` FOREIGN KEY (`username`) REFERENCES `User` (`username`),
  ADD CONSTRAINT `Publisher_Add_Request_ibfk_2` FOREIGN KEY (`admin_username`) REFERENCES `User` (`username`);

--
-- Constraints for table `Rates`
--
ALTER TABLE `Rates`
  ADD CONSTRAINT `Rates_ibfk_1` FOREIGN KEY (`username`) REFERENCES `User` (`username`),
  ADD CONSTRAINT `Rates_ibfk_2` FOREIGN KEY (`id_game`) REFERENCES `Game` (`id_game`);

--
-- Constraints for table `Sessions`
--
ALTER TABLE `Sessions`
  ADD CONSTRAINT `sessions_ibfk_1` FOREIGN KEY (`username`) REFERENCES `User` (`username`);
